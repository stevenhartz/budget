<?php
defined('BUDGET') or die('access denied');

class Config{
    const pw_min_len = 8;
    const pw_max_len = 64;
    const db_dir = '/var/www/db';
    const login_attempts = 5; // login attempts before a lockout <--NOT IMPLEMENTED-->
    const lockout_time = 15*60; // lockout after $login_attempts failures, in seconds <--NOT IMPLEMENTED-->

    const session_lifetime = 30*60; // seconds, session will expire if inactive for this long
    const session_max_lifetime = 2*60*60; // seconds, sessions will always expire after this much time

    const csrf_token_len = 16; // bytes
    const csrf_token_lifetime = 30*60; // seconds


    // Database Encryption
    const pbkdf2_algo = 'sha256';
    const pbkdf2_iterations = 1000000;
    const db_crypt_cipher = 'aes-128-gcm';

    // Password stuff
    const argon2_memory_cost = 1024*1024; // KiB, Found with hashing_calibration.php
    const argon2_time_cost = 10; // Found with hashing_calibration.php
    const argon2_parallelism = 12; // Threads available on your CPU
}
