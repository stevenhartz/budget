<?php
define('BUDGET',true);
require_once('lib/load_all.php');

$prompt = 'totp';
if(isset($_POST['login'])) {

    if(CSRF::checkToken($_POST['csrf'])){

        if(isset($_POST['totp'])){
            // TODO: Lockouts
            if(User::checkTOTP($_POST['username'], $_POST['totp'])){
                $prompt = 'passphrase';
            }else{
                $loginError = 'Login Failed';
            }
        }
        elseif(User::isTOTPSolved()){
            // Leave this set so user can try again if it's incorrect.
            // TODO: lockouts
            $prompt = 'passphrase';

            $u = User::getCurrentUser();

            try{
                $db = DB::unlockAndGetTransactDB($u, $_POST['passphrase']);
                
                // session_regenerate_id() causes the session to be serialized.
                // this causes the key to be wiped because of Session::expireCurrent().
                // So we need to preserve the key before and after the session rotation.
                $key = $_SESSION['key'];
                session_regenerate_id();
                $_SESSION['key'] = $key;
                
                Logger::getLogger()->logDebug("user session rotated");
                header('Location: /dashboard.php');
                exit();
            }catch(DbDecryptFailedException $e){
                $loginError = 'Failed to unlock database';
            }

        }
    }else{
        $loginError = 'Page timed out, please try again.';
    }

}

if(isset($_GET['exp'])){
    if($_GET['exp'] == Session::INACTIVE_EXPIRE){
        $sessionExpire = 'Session expired due to inactivity. Please login again.';
    }elseif($_GET['exp'] == Session::ABSOLUTE_EXPIRE){
        $sessionExpire = 'Maximum session length exceeded. Please login again.';
    }
}

if(User::isLoggedIn()){
    header('Location: /dashboard.php');
    exit();
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Budget</title>
    <?php require('reuseables/unauth_styles_scripts.php') ?>
</head>
<body>
    <?php require('reuseables/header.php') ?>
    <div id="container">
        <div id="login">
            <h2>Login</h2>
            
            <?php if(isset($loginError)){
                echo $loginError;
            } ?>
            <?php if(isset($sessionExpire)){
                echo $sessionExpire;
            } ?>
            
            <?php if($prompt == 'totp'){ ?>
                <form action="" method="post">
                    <input type="text" name="username" placeholder="Username" />
                    <input type="text" name="totp" placeholder="2FA: 012345" />
                    <input type="hidden" name="csrf" value="<?php echo CSRF::getToken() ?>" />

                    <input type="submit" name="login" value="Log in" />
                </form>
                New user? <a href="/register.php">Register!</a>
            <?php 
            }
            elseif($prompt == 'passphrase') { ?>
                <p>Please enter your passphrase/database key.</p>
                <form action="" method="post">
                    <input type="password" name="passphrase" placeholder="passphrase" />
                    <input type="hidden" name="time_offset" id="time_offset" />
                    <script>
                        let tz = document.getElementById('time_offset');
                        tz.value = (new Date()).getTimezoneOffset();
                    </script>
                    <input type="hidden" name="csrf" value="<?php echo CSRF::getToken() ?>" />

                    <input type="submit" name="login" value="Log in" />
                </form>
            <?php
            }
            else{ ?>
                <p>An unknown error occured. <a href="">Start over</a></p>
            <?php } ?>
        </div>
    </div>
    <?php require('reuseables/footer.php') ?>
</body>
</html>