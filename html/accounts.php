<?php
define('BUDGET',true);
require_once('lib/load_all.php');

$DATE_FORMAT = 'M d, Y';

// This is a bad workaround for timezone correction.
// IE does not support the ECMA `Intl` API. So here's our browser-agnostic workaround.
$tz_offset = $_SESSION['tz_offset_seconds'];

if(!User::isLoggedIn()){
    header('Location: /index.php');
}else{
    $u = User::getCurrentUser();
}
require_model_once('account');

if(isset($_POST['submit']) || isset($_POST['submit_x'])){

    //TODO: fix assumption that everything is set

    $errors = array();
    if(strlen($_POST['name']) == 0){
        $errors[] = "Category name blank";
    }
    

    if(count($errors) == 0){

        if($_POST['id'] == 0){
            // id=0 means add new
            // Balance is only valid for new acconuts, so we need to check this again here.
            $balance = round($_POST['balance'] * 100);

            if(!preg_match('/^\\d+(\\.\\d\\d)?$/', $_POST['balance'])){
                $errors[] = "Balance value invalid, should resemble '123' or '12.34'";
            }else{
                $account = new Account($_POST['name'], null, $balance, time());
                echo '<!-- saving new -->';
                $account->save();
            }
        }else{
            $account = Account::getAccount($_POST['id']);
            $account->name = $_POST['name'];
            echo '<!-- saving existing -->';
            $account->save();
        }
    }


}elseif(isset($_POST['delete']) || isset($_POST['delete_x'])){
    $account = Account::getAccount($_POST['id']);
    $account->delete();
}

require('reuseables/categories.php'); 
?>
<!DOCTYPE html>	
<html>
<head>
    <title>Budget</title>
    <?php require('reuseables/unauth_styles_scripts.php') ?>
    <?php require('reuseables/auth_styles_scripts.php') ?>
</head>
<body class="darkmode">
    <header>
        <div id="logo">
            <a href="/"><h1>Budget</h1></a>
        </div>
        <div id="profile">
            <ul>
                <li>Welcome, <?php echo htmlentities($u->getUsername()) ?></li>
                <li><a href="profile.php">Profile</a></li>
                <li><a href="logout.php">Log Out</a></li>
            </ul>
        </div>
        <div class="clear"></div>
    </header>
    <div id="container">
        <div id="tabs">
            <ul id="tab-nav">
                <li>
                    <a href="/dashboard.php">
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="/transactions.php">
                        <span>Transactions</span>
                    </a>
                </li>
                <li class="selected">
                    <a href="/accounts.php">
                        <span>Accounts</span>
                    </a>
                </li>
                <li>
                    <a href="/goals.php">
                        <span>Goals</span>
                    </a>
                </li>
                <li>
                    <a href="/categories.php">
                        <span>Categories</span>
                    </a>
                </li>
                <div class="clear"></div>
            </ul>
        </div>
        <div id="main" class="category-page">
            <?php
            if(isset($errors) && count($errors) > 0){
                ?>
                <div style="position:absolute; top:0; left: 45%; background-color: red;">
                    <?php foreach($errors as $error){
                        echo '<p>'.$error.'</p>';
                    }?>
                </div>
                <?php
            }?>
            <div>
                <div id="expenses">
                    <h2>Accounts</h2>
                    <div class="table">
                        <div class="tr">
                            <div class="th category">Account Name</div>
                            <div class="th">Starting Balance</div>
                            <div class="th">Starting Date</div>
                            <div class="th">Calculated Balance</div>
                            <div class="th"></div>
                        </div>
                <?php
                    $accounts = Account::getAccountsAndBalances();
                    foreach($accounts as $account){
                        ?>

                        <form class="tr" action="" method="POST">
                            <div class="td category">
                                <input type="text" name="name" value="<?php echo htmlentities($account->name) ?>" />
                            </div>
                            <div class="td">
                                <?php $fixedPointBalance = $account->getBalance();
                                $balance = sprintf("%.2f", $fixedPointBalance / 100.0);
                                
                                // TODO: What's the best UI for updating balances?
                                // leaving this until I can think about the best solution for balance + date/time
                                echo htmlentities($balance) ?>
                            </div>
                            <div class="td">
                                <?php echo date($DATE_FORMAT, $account->getLastUpdate() - $tz_offset) ?>
                            </div>
                            <div class="td">
                                (To Do)
                            </div>
                            
                            <div class="td">
                                <input type="image" name="submit" src="/img/icons/update.svg" class="icon"  title="Update"/>
                                <input type="image" name="delete" src="/img/icons/delete.svg" class="icon"  title="Delete"/>
                                <input type="hidden" name="id" value="<?php echo $account->getId() ?>" />
                                <input type="hidden" name="type" value="expense" />
                            </div>
                        </form>
                    
                        
                        <?php
                    }   ?>
                        
                        <div class="tr">
                            <div class="td header-add">Add New</div>
                        </div>
                        <form class="tr" action="" method="POST">
                            <div class="td category">
                                <?php if(isset($errors) && count($errors) > 0 && $_POST['id']==0){ ?>
                                    <input type="text" name="name" value="<?php echo htmlentities($_POST['name']) ?>" />
                                <?php }else{ ?>
                                    <input type="text" name="name" placeholder="Account Name" />
                                <?php } ?>
                            </div>
                            <div class="td">
                                <?php if(isset($errors) && count($errors) > 0 && $_POST['id']==0){ ?>
                                    <input type="text" name="balance" value="<?php echo htmlentities($_POST['balance']) ?>" />
                                <?php }else{ ?>
                                    <input name="balance" type="text" placeholder="0.00" />
                                <?php } ?>
                            </div>

                            <div class="td">
                                <?php echo date($DATE_FORMAT, time() - $tz_offset) ?>
                            </div>

                            <div class="td">
                            </div>
                            
                            <div class="td">
                                <input type="image" name="submit" src="/img/icons/add.svg" class="icon"  title="Add"/>
                                <input type="hidden" name="id" value="0" />
                                <input type="hidden" name="type" value="expense" />
                            </div>
                        </form>
                    
                    </div>

                </div>
            </div>
           
        </div>
    </div>
    <script type="text/javascript" src="/scripts/dashboard.js"></script>
</body>
</html>