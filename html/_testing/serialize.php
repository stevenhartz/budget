<?php

$db = new SQLite3(':memory:');
$db->exec('CREATE TABLE Categories (
    id INTEGER NOT NULL PRIMARY KEY,
    name TEXT NOT NULL
);');

$db->exec('INSERT INTO Categories (id, name) VALUES(null, "some string1")');
$db->exec('INSERT INTO Categories (id, name) VALUES(null, "some string2")');
$db->exec('INSERT INTO Categories (id, name) VALUES(null, "some string3")');
$db->exec('INSERT INTO Categories (id, name) VALUES(null, "some string4")');


$r = $db->query('SELECT * FROM Categories');
if(is_null($r)){
    echo 'Is null!';
}else{
    $data = $r->fetchArray();
    var_dump($data);
}

$s = serialize($db);
echo 'serialized!';