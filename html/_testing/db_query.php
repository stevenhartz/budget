<?php
define('BUDGET',true);
require_once('../lib/load_all.php');

if(!User::isLoggedIn()){
    die('Not logged in!');
}

echo '<h1 style="color:red">DISABLE THIS PAGE FOR PRODUCTION INSTANCES</h1>';

echo '<form action="" method="post">';
echo '<textarea name="sql" cols="100" rows="10">';
if(isset($_POST['sql'])){
    echo $_POST['sql'];
}
echo '</textarea>';
echo '<br />';
echo '<input type="submit" />';
echo '</form>';

$db = DB::getTransactDB(User::getCurrentUser());

if(isset($_POST['sql'])){
    echo '<p>Results:</p>';
    echo '<pre>';
    $r = $db->query($_POST['sql']);

    $cols = array();
    for($i=0; $i < $r->numColumns(); $i++){
        $cols[] = $r->columnName($i);
    }
    echo implode(' | ', $cols) , "\n";

    while($row = $r->fetchArray(true)){
        
        echo implode(" | ", $row) , "\n";
    }
}