<?php
define('BUDGET',true);
require_once('../lib/load_all.php');

if(!User::isLoggedIn()){
    die('Not logged in!');
}

$dump = DB::export(DB::getTransactDB(User::getCurrentUser()));

echo '<pre>';
echo var_dump($dump);
echo '</pre>';