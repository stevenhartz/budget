<?php
define('BUDGET',true);
require_once('lib/load_all.php');

/**
 * This primary purpose of this page is to let the Chase browser
 * plugin know that the user is logged in and that the app is up
 * and running.
 */

// we need to set CORS for the plugin
// This is a mild security risk: any website can tell if users are logged in.
// I'm accepting this risk, but an idea solution would be to use PKI to
// identify the client.
$headers = getallheaders();
$origin = $headers['origin'];

header('Access-Control-Allow-Origin: ' . $origin);
header('Access-Control-Allow-Credentials: true');

if(User::isLoggedIn()){
    echo 'true';
}else{
    echo 'false';
}