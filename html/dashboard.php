<?php
define('BUDGET',true);
require_once('lib/load_all.php');

if(!User::isLoggedIn()){
    header('Location: /index.php');
}else{
    $u = User::getCurrentUser();

    try{
        $db = DB::getTransactDB($u);
        if(is_null($db)) echo 'db is null';
    }catch(DbNotUnlockedException $e){
        echo 'Not unlocked!';
        header('Location: /unlock_db.php');
    }catch(DbDecryptFailedException $e){
        die('Database failed to decrypt or is corrupt');
    }

}
require_once('reuseables/categories.php'); 
?>
<!DOCTYPE html>	
<html>
<head>
    <title>Budget</title>
    <?php require('reuseables/unauth_styles_scripts.php') ?>
    <?php require('reuseables/auth_styles_scripts.php') ?>
</head>
<body class="darkmode">
    <header>
        <div id="logo">
            <a href="/"><h1>Budget</h1></a>
        </div>
        <div id="profile">
            <ul>
                <li>Welcome, <?php echo htmlentities($u->getUsername()) ?></li>
                <li><a href="profile.php">Profile</a></li>
                <li><a href="logout.php">Log Out</a></li>
            </ul>
        </div>
        <div class="clear"></div>
    </header>
    <div id="container">
        <div id="tabs">
            <ul id="tab-nav">
                <li class="selected">
                    <a href="/dashboard.php">
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="/transactions.php">
                        <span>Transactions</span>
                    </a>
                </li>
                <li>
                    <a href="/accounts.php">
                        <span>Accounts</span>
                    </a>
                </li>
                <li>
                    <a href="/goals.php">
                        <span>Goals</span>
                    </a>
                </li>
                <li>
                    <a href="/categories.php">
                        <span>Categories</span>
                    </a>
                </li>
                <div class="clear"></div>
            </ul>
        </div>
        <div id="main" class="dashboard-page">
            <div class="left dash-column">
                <div id="expenses">
                    <h2>Expenses</h2>
                    <?php
                        echo_categories_table(true, true);
                    ?>
                </div>
                <div id="expense_graph_pie">
                    <h3>Spending By Category</h3>
                    <div style="background:grey; height:400px; width:100%"></div>
                </div>
            </div>
            <div class="right dash-column">
                <div id="income">
                    <h2>Income</h2>
                    <?php
                        echo_categories_table(false, true);
                    ?>
                </div>
                <div id="expenses_graph_bar">
                    <h3>Spending By Category</h3>
                    <div style="background:grey; height:400px; width:100%"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <script type="text/javascript" src="/scripts/dashboard.js"></script>
</body>
</html>