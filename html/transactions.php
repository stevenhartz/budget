<?php
define('BUDGET',true);
require_once('lib/load_all.php');

$DATE_FORMAT = 'M d, Y';

// This is a bad workaround for timezone correction.
// IE does not support the ECMA `Intl` API. So here's our browser-agnostic workaround.
$tz_offset = $_SESSION['tz_offset_seconds'];

if(!User::isLoggedIn()){
    header('Location: /index.php');
}else{
    $u = User::getCurrentUser();
}

$db = DB::getUserTransactionDB();
require_model_once('category');
require_model_once('account');
require_model_once('transaction');


if(isset($_POST['submit']) || isset($_POST['submit_x'])){

    //TODO: fix assumption that everything is set (partially done)

    $errors = array();

    if(!preg_match('/^\\d+(\\.\\d\\d)?$/', $_POST['income'])){
        $income = null;
    }else{
        $income = round($_POST['income'] * 100);
    }
    if(!preg_match('/^\\d+(\\.\\d\\d)?$/', $_POST['expense'])){
        $expense = null;
    }else{
        $expense = round($_POST['expense'] * 100);
    }

    if(is_null($income) && is_null($expense)){
        $errors[] = 'Specify either income or expense, like 123 or 123.45';
    }elseif(!is_null($income) && !is_null($expense)){
        $errors[] = 'Specific either income or expense, not both';
    }else{

        // Exactly one of $expense or $income was set and valid.
        if(is_null($expense)){
            $expense = -1 * $income;
        }
    }

    if($_POST['id'] == 0){
        // id=0 means add new    



        if(!isset($_POST['category']) || !is_numeric($_POST['category'])){
            $errors[] = 'Category not set';
        }

        
        if(count($errors) == 0){

            $account = Account::getAccount($_POST['account']);
            $category = Category::getCategory($_POST['category']);
            $transaction = new Transaction($account, $expense, $category, $_POST['tag'], null, null);

            $transaction->save();

        }


    }else{
        echo '<!-- id: ',$_POST['id'],' -->';
        $transaction = Transaction::getTransaction($_POST['id']);
        echo '<!--';
        print_r($transaction);
        echo '-->';
        $account = Account::getAccount($_POST['account']);
        //$category = Category::getCategory($_POST['category']);
        
        $transaction->setAccount($account);
        //$transaction->setCategory($category);
        $transaction->setExpense($expense);
        $transaction->tags = $_POST['tags'];

        $transaction->save();
    }


}elseif(isset($_POST['delete']) || isset($_POST['delete_x'])){
    Transaction::getTransaction($_POST['id'])->delete();
}

require('reuseables/categories.php'); 

// we'll use this multiple times later
$accounts = Account::getAccounts();
?>
<!DOCTYPE html>	
<html>
<head>
    <title>Budget</title>
    <?php require('reuseables/unauth_styles_scripts.php') ?>
    <?php //require('reuseables/auth_styles_scripts.php') ?>
</head>
<body class="darkmode">
    <header>
        <div id="logo">
            <a href="/"><h1>Budget</h1></a>
        </div>
        <div id="profile">
            <ul>
                <li>Welcome, <?php echo htmlentities($u->getUsername()) ?></li>
                <li><a href="profile.php">Profile</a></li>
                <li><a href="logout.php">Log Out</a></li>
            </ul>
        </div>
        <div class="clear"></div>
    </header>
    <div id="container">
        <div id="tabs">
            <ul id="tab-nav">
                <li>
                    <a href="/dashboard.php">
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="selected">
                    <a href="/transactions.php">
                        <span>Transactions</span>
                    </a>
                </li>
                <li>
                    <a href="/accounts.php">
                        <span>Accounts</span>
                    </a>
                </li>
                <li>
                    <a href="/goals.php">
                        <span>Goals</span>
                    </a>
                </li>
                <li>
                    <a href="/categories.php">
                        <span>Categories</span>
                    </a>
                </li>
                <div class="clear"></div>
            </ul>
        </div>
        <div id="main" class="category-page">
            <?php
            if(isset($errors) && count($errors) > 0){
                ?>
                <div style="position:absolute; top:0; left: 45%; background-color: red;">
                    <?php foreach($errors as $error){
                        echo '<p>'.$error.'</p>';
                    }?>
                </div>
                <?php
            }?>
            <div>
                <div id="expenses">
                    <h2>Transactions</h2>
                    <div class="table">
                        <div class="tr">
                            <div class="th category">Account Name</div>
                            <div class="th">Expense</div>
                            <div class="th">Income</div>
                            <div class="th txtcol">Category</div>
                            <div class="th txtcol">Sub-Category</div>
                            <div class="th txtcol">Tag</div>
                            <div class="th txtcol">Correlation</div>
                            <div class="th"><!-- Controls --></div>
                        </div>
                <?php
                    $transactions = Transaction::getTransactions();
                    foreach($transactions as $transaction){                    

                        ?>

                        <form class="tr" action="" method="POST">
                            <div class="td category">
                                <select name="account">
                                    <?php
                                        $id = $transaction->getAccount()->getId();
                                        foreach($accounts as $account){
                                            $encodedAccount = htmlentities($account->name);
                                            if($id == $account->getId()){
                                                echo "<option value='{$account->getId()}' selected='selected'>{$encodedAccount}</option>";
                                            }else{
                                                echo "<option value='{$account->getId()}'>{$encodedAccount}</option>";
                                            }
                                        } 
                                    ?>
                                </select>
                            </div>
                            <div class="td">
                                <?php
                                    $fixedPointExpense = $transaction->getExpense();
                                    $expense = sprintf("%.2f", $fixedPointExpense / 100.0);
                                    
                                    // TODO: What's the best UI for updating balances?
                                    // leaving this until I can think about the best solution for balance + date/time
                                    $txtExpense = ($expense >= 0 ? htmlentities($expense) : '');
                                    echo "<input name='expense' type='text' value='{$txtExpense}'>";
                                ?>
                            </div>
                            <div class="td">
                                <?php 
                                    $expense = sprintf("%.2f", -1 * $fixedPointExpense / 100.0);
                                    $txtExpense = ($expense > 0 ? htmlentities($expense) : '');
                                    echo "<input name='income' type='text' value='{$txtExpense}'>";
                                ?>
                            </div>
                            <div class="td txtcol">
                                <?php
                                echo htmlentities($transaction->getCategory()->getName());
                                ?>
                            </div>

                            <div class="td txtcol">
                                <!-- subCategory -->
                            </div>

                            <div class="td txtcol">
                                <input type="text" name="tags" value="<?php echo htmlentities($transaction->tags) ?>" />
                            </div>

                            <div class="td txtcol">
                                <!-- asdf -->
                            </div>
                            
                            <div class="td">
                                <input type="image" name="submit" src="/img/icons/update.svg" class="icon"  title="Update"/>
                                <input type="image" name="delete" src="/img/icons/delete.svg" class="icon"  title="Delete"/>
                                <input type="hidden" name="id" value="<?php echo $transaction->getId() ?>" />
                                <input type="hidden" name="type" value="expense" />
                            </div>
                        </form>
                    
                        
                        <?php
                    }   ?>
                        <div class="tr">
                            <div class="td header-add">Add New</div>
                        </div>
                        <form class="tr" action="" method="POST">
                            <div class="td txtcol">
                                <select name="account">
                                    <option value=""></option>
                                <?php
                                    foreach($accounts as $account){
                                        $encodedAccount = htmlentities($account->name);
                                        echo "<option value='{$account->getId()}'>{$encodedAccount}</option>";
                                    } 
                                ?>
                                </select>
                            </div>
                            <div class="td">
                                <?php if(isset($errors) && count($errors) > 0 && $_POST['id']==0){ ?>
                                    <input name="expense" type="text" value="<?php echo htmlentities($_POST['expense']) ?>" />
                                <?php }else{ ?>
                                    <input name="expense" type="text" placeholder="0.00" />
                                <?php } ?>
                            </div>
                            <div class="td">
                                <?php if(isset($errors) && count($errors) > 0 && $_POST['id']==0){ ?>
                                    <input name="income" type="text" value="<?php echo htmlentities($_POST['income']) ?>" />
                                <?php }else{ ?>
                                    <input name="income" type="text" placeholder="0.00" />
                                <?php } ?>
                            </div>

                            <?php
                                $q = 'SELECT id, name, expense FROM Categories ORDER BY expense DESC, id';
                                $r = $db->query($q);
                            ?>
                            <div class="td txtcol">
                                <select name="category">
                                    <?php
                                        $expCats = Category::getExpenseCategories();
                                        $incCats = Category::getIncomeCategories();
                                        
                                        echo '<optgroup label="Expenses">';
                                        foreach($expCats as $cat){
                                            $txt = htmlentities($cat->getName());
                                            echo "<option value='{$cat->getId()}'>{$txt}</option>";
                                        }
                                        echo '</optgroup>';

                                        echo '<optgroup label="Income">';
                                        foreach($incCats as $cat){
                                            $txt = htmlentities($cat->getName());
                                            echo "<option value='{$cat->getId()}'>{$txt}</option>";
                                        }
                                        echo '</optgroup>';
                                    ?>
                                </select>
                            </div>

                            <div class="td txtcol">
                                <select name="sub_cat">
                                </select>
                            </div>

                            <div class="td txtcol">
                                <input name="tag" type="text" placeholder="list,of,tags" />
                            </div>

                            <div class="td txtcol">
                            </div>
                            
                            <div class="td">
                                <input type="image" name="submit" src="/img/icons/add.svg" class="icon"  title="Add"/>
                                <input type="hidden" name="id" value="0" />
                                <input type="hidden" name="type" value="expense" />
                            </div>
                        </form>
                    
                    </div>

                </div>
            </div>
           
        </div>
    </div>
    <script type="text/javascript" src="/scripts/dashboard.js"></script>
</body>
</html>