(function(){

    let ApiUrls={
        addBudgetRow: "/api/budget/add_category.php",
        deleteBudgetRow: "/api/budget/delete_category.php",
        editBudgetRow: "/api/budget/edit_category.php"
    }

    console.log(ApiUrls)

    $ = function(selector){
        return document.querySelector(selector);
    }

    // Hooks up the <input> and hidden <spans> so that the inputs resize magically
    // cssStub is a class/id stub that is used to identify input-span pairs
    function registerInput(inputObj, spanObj, updateIconObj=null){
        inputObj.oninput = function(e, register=false){
            console.log(`onInput(${e},${register})`)
            spanObj.textContent = this.value
            this.style.width = spanObj.offsetWidth + 2 + "px"
            console.log('onInput')

            // update and remove icons will be null on the add row
            if(!register && updateIconObj != null){
                updateIconObj.removeAttribute('disabled')
                updateIconObj.classList.remove('icon_disabled')
            }

        }

        inputObj.oninput.apply(inputObj, Array(inputObj, true))
    }

    // Registers the table of inputs
    function registerInputs(cssStub1, cssStub2){
        
        console.log(`Registering: $(.edit_text_${cssStub1}_${cssStub2})`)
        Array.from(document.querySelectorAll(`.edit_text_${cssStub1}_${cssStub2}`)).forEach(function(input){
            console.log(`....input.id`)
            let number = input.id.match(/[0-9]+/)[0]
            
            let hidden = $(`#hidden_${cssStub1}_${cssStub2}${number}`)
            let updateIcon = $(`#update_${cssStub1}_row${number}`)

            registerInput(input, hidden, updateIcon);

            console.log('applying')
        })
    }
    registerInputs('budget','cat');
    registerInputs('budget','num');
    registerInputs('income','cat');
    registerInputs('income','num');

    registerInput($('#text_budget_cat0'), $('#hidden_budget_cat0'))
    registerInput($('#text_income_cat0'), $('#hidden_income_cat0'))
    registerInput($('#text_budget_num0'), $('#hidden_budget_num0'))
    registerInput($('#text_income_num0'), $('#hidden_income_num0'))

    $('#add_budget_row0').onclick = function(e){

        if(!this.hasAttribute('disabled')){
            this.classList.add('icon_disabled')
            this.setAttribute('disabled','disabled')
            fetch(ApiUrls.addBudgetRow, {
                method: 'POST',
                body: JSON.stringify({
                    csrf: localStorage.getItem('csrf'),
                    name: $('#text_budget_cat0').value,
                    budget: $('#text_budget_num0').value
                })
            }).then(resp => {
                console.log('response')
                resp.json().then(data => {
                    if(data['result'] == 'success'){
                        console.log(`[Add Budget Category] Success (id: ${data['id']})`)
                        this.classList.remove('icon_disabled')
                        this.removeAttribute('disabled')
                    }else{
                        this.classList.remove('icon_disabled')
                        this.removeAttribute('disabled')
                        console.log(`[Add Budget Category] Failure (${data['error_message']})`)
                        
                    }
                }).catch(e => {
                    console.log(e)
                })
            })
        }
    }

})()