
(function(){
    let ApiUrls={
        csrf: "/api/get_csrf.php"
    }

    $ = function(selector){
        return document.querySelector(selector);
    }

    let toast = function(message){
        console.log(`[Toast] ${message}`)
    }

    /*
    $('#add-budget-row').onclick = function(){
        fetch(ApiUrls.addBudgetRow, {
            method: 'POST',
            body: JSON.stringify({
                    name: "test",
                    budget: "123.45"
            })
        }).then(toast("Saved!")).catch(function(e){
            toast("Error")
            console.error(e);
            //remove row?
        })
    }
    */

    fetch(ApiUrls.csrf).then(function(resp){
        resp.json().then(function(data){

            console.info(data)

            if(data['error']){
                console.error('Failed to get CSRF (unexpected response)')
            }else{
                console.log('CSRF token ok')
                localStorage.setItem('csrf', data['csrf'])
            }
        })
    }).catch(function(){
        console.error('Failed to get CSRF (network error)')
    })

})();