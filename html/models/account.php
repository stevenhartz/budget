<?php
defined('BUDGET') or die('access denied1');
require_once(__DIR__ . '/../lib/load_all.php');
require_model_once('category');
require_model_once('account');

// Make sure there's an open DB object
// Do not attempt recovery here; make the caller deal with it.
$db = DB::getUserTransactionDB();

class AccountNotFoundException extends Exception { }

class Account{

    protected static $db = null;

    protected int $id;
    protected ?string $externalId; // Only gettable for now. Add extending class for external accounts?
    protected ?int $balance;
    protected ?int $lastUpdate;

    public string $name;

    public function __construct(string $name, ?string $externalId=null, int $balance=0, int $lastUpdate=-1){
        $this->id = 0;
        $this->name = $name;
        $this->externalId = $externalId;

        $this->balance = $balance;
        $this->lastUpdate = ($lastUpdate<0 ? time() : $lastUpdate);
    }

    protected static function constructFromDbRow($row): Account{
        $acct = new Account($row['name'], $row['external_id']);
        $acct->id = $row['id'];

        return $acct;
    }

    public static function getDB(): DB{
        if(is_null(Self::$db)){
            Self::$db = DB::getUserTransactionDB();
        }
        return Self::$db;
    }

    public static function getAccounts(): array{
        $db = Self::getDB();

        $stmt = $db->prepare('SELECT * FROM Accounts');
        $r = $stmt->execute();

        $accts = array();
        while($row = $r->fetchArray(true)){
            $accts[] = Self::constructFromDbRow($row);
        }

        return $accts;
    }

    public static function getAccountsAndBalances(): array{
        $db = Self::getDB();

        $q = 'SELECT Accounts.id as id, name, external_id, MAX(date_ts) as date_ts, balance, manually_entered FROM Accounts JOIN Balances ON Balances.account_id = Accounts.id GROUP BY Accounts.id';
        $r = $db->query($q);
        
        $accts = array();
        while($row = $r->fetchArray(true)){
            $acct = Self::constructFromDbRow($row);
            $acct->lastUpdate = $row['date_ts'];
            $acct->balance = $row['balance'];

            $accts[] = $acct;
        }

        return $accts;
    }

    public static function getAccount(int $id): Account{
        $db = Self::getDB();

        $stmt = $db->prepare('SELECT * FROM Accounts WHERE id=:id');
        $stmt->bindValue('id', $id);
        $r = $stmt->execute();

        $row = $r->fetchArray(true);
        if($row === false){
            throw new AccountNotFoundException("Account id {$id} not found");
        }

        $acct = Self::constructFromDbRow($row);
        return $acct;
    }

    public function getId(): int{
        return $this->id;
    }

    public function externalId(): string{
        return $this->externalId;
    }

    private function calcBalanceAndDate(): void{
        $db = Self::getDB();
        $stmt = $db->prepare('SELECT * FROM Balances WHERE account_id=:account_id ORDER BY date_ts DESC LIMIT 1');
        $stmt->bindValue(':account_id', $this->id);

        $r = $stmt->execute();
        $row = $r->fetchArray(true);

        if($row !== false){
            $this->balance = $row['balance'];
            $this->lastUpdate = $row['date_ts'];
        }else{
            // Promise to never return null
            // although I'm not sure this is a better plan
            $this->balance = 0;
            $this->lastUpdate = time();
        }
    }

    public function getLastUpdate(): int{
        
        if(is_null($this->lastUpdate)){
            $this->calcBalanceAndDate();
        }
        return $this->lastUpdate;
    }

    public function getBalance(): int{
        
        if(is_null($this->balance)){
            $this->calcBalanceAndDate();
        }
        return $this->balance;
    }

    public function save(): void{
        if($this->id == 0){
            $this->add();
        }else{
            $this->update();
        }
    }

    public function add(): void{
        $db = Self::getDB();

        //--------------
        $db->query('BEGIN TRANSACTION');
        // TODO: handle external account id
        $stmt = $db->prepare('INSERT INTO Accounts (id, name, external_id) VALUES (null, :name, "")');
        $stmt->bindValue(':name', $this->name);
        $result1 = $stmt->execute();

        $this->id = $db->lastInsertRowID();

        $stmt = $db->prepare('INSERT INTO Balances (id, account_id, balance, date_ts, manually_entered)
                                            VALUES (null, :acctId, :balance, :ts, 1)');
        $stmt->bindValue(':acctId', $this->id);
        $stmt->bindValue(':balance', $this->balance);
        $stmt->bindValue(':ts', $this->lastUpdate);
        $result2 = $stmt->execute();

        if($result1 && $result2){
            $db->query('COMMIT');
        }else{
            $db->query('ROLLBACK');
        }
    }

    public function update(): void{
        $db = Self::getDB();
        
        $stmt = $db->prepare('UPDATE Accounts SET name=:name, external_id=:external_id WHERE id=:id');
        $stmt->bindValue(':id', $this->id);
        $stmt->bindValue(':name', $this->name);
        $stmt->bindValue(':external_id', $this->externalId);

        $stmt->execute();
    }

    public function delete(): void{
        $db = Self::getDB();

        $stmt = $db->prepare('DELETE FROM Accounts WHERE id=:id');
        $stmt->bindValue(':id', $this->id);
        
        $stmt->execute();
    }

    
}