<?php
defined('BUDGET') or die('access denied1');
require_once(__DIR__ . '/../lib/load_all.php');


class CategoryNotFoundException extends Exception {}

class Category{

    // standing in for enum, which doesn't exist in older versions of PHP :(
    const Expense = 0;
    const Income = 1;
    
    protected static $db = null;

    protected $id;
    protected $catName;
    protected $budget;
    protected $catType;

    function __construct(string $catName, int $budget, int $type){
        

        $this->id = null;
        $this->catName = $catName;
        $this->budget = $budget;

        // let this method handle sanity checks
        $this->setType($type);
    }

    protected static function constructFromDbRow($row){
        $type = ($row['expense'] == true ? Self::Expense : Self::Income);
        $cat = new Category($row['name'], $row['budget'], $type);
        $cat->id = $row['id'];

        return $cat;
    }

    public function save(){
        if(is_null($this->id)){
            $this->add();
        }else{
            $this->update();
        }
    }

    public function add(){
        $db = Self::getDB();

        $stmt = $db->prepare('INSERT INTO Categories (id, name, budget, expense) VALUES (null, :name, :budget, :expense)');
        $stmt->bindValue(':name', $this->catName);
        $stmt->bindValue(':budget', $this->budget);
        $stmt->bindValue(':expense', ($this->catType == Self::Expense));

        $stmt->execute();
    }

    public function update(){
        $db = Self::getDB();
        
        $stmt = $db->prepare('UPDATE Categories SET name=:name, budget=:budget, expense=:expense WHERE id=:id');
        $stmt->bindValue(':id', $this->id);
        $stmt->bindValue(':name', $this->catName);
        $stmt->bindValue(':budget', $this->budget);
        $stmt->bindValue(':expense', ($this->catType == Self::Expense));

        $stmt->execute();
    }

    public static function getDB(){
        if(is_null(Self::$db)){
            Self::$db = DB::getUserTransactionDB();
        }

        return Self::$db;
    }
    
    public static function getCategory(int $id){
        $db = Self::getDB();

        $stmt = $db->prepare('SELECT * FROM Categories WHERE id=:id');
        $stmt->bindValue(':id', $id);
        $r = $stmt->execute();

        $row = $r->fetchArray(true);
        if($row === false){
            throw new CategoryNotFoundException('Category id not found');
        }

        $cat = Self::constructFromDbRow($row);
        return $cat;
    }

    public static function getAllCategories(){
        return Self::getCategories();
    }

    public static function getExpenseCategories(){
        return Self::getCategories(['expense', true]);
    }

    public static function getIncomeCategories(){
        return Self::getCategories(['expense', false]);
    }

    protected static function getCategories(array $filter=null){
        $db = Self::getDB();

        $q = 'SELECT * FROM Categories';
        if(!is_null($filter)){
            if(!in_array($filter[0], ['id','name','budget','expense'])){
                throw new Exception("Filter column not in table, potential SQLi. (col: {$filter[0]}");
            }
            $q .= ' WHERE ' . $filter[0] . '=:'.$filter[0];
        }

        $stmt = $db->prepare($q);
        if(!is_null($filter)){
            $stmt->bindValue(':'.$filter[0], $filter[1]);
        }
        $r = $stmt->execute();

        $categories = array();
        while($row = $r->fetchArray(true)){
            $categories[] = Self::constructFromDbRow($row);
        }

        return $categories;
    }

    public static function deleteCategory(int $id){
        $db = Self::getDB();

        $stmt = $db->prepare('DELETE FROM Categories WHERE id=:id');
        $stmt->bindValue(':id', $id);
        
        $stmt->execute();
    }

    public function getId(): int{
        return $this->id;
    }

    public function getName(): string{
        return $this->catName;
    }

    public function getBudget(): int{
        return $this->budget;
    }

    public function getType(): int{
        return $this->catType;
    }

    public function setName(string $name){
        $this->catName = $name;
    }

    public function setBudget(int $budget){
        $this->budget = $budget;
    }

    public function setType(int $type){
        
        if($type !== Self::Expense && $type !== Self::Income){
            throw new Exception("Invalid Type", 1);
        }

        $this->catType = $type;
    }
}