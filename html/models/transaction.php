<?php
defined('BUDGET') or die('access denied2');
require_once(__DIR__ . '/../lib/load_all.php');
require_model_once('category');
require_model_once('account');

class Transaction{

    protected static $db = null;

    protected int $id;
    protected ?int $externalId; // external transaction ID to deconflict with import from, e.g., Chase
    protected Account $account;
    protected int $expense; // negative values mean income
    protected Category $category;
    protected ?int $subCategoryId; // TODO: Subcategory
    protected bool $isTransfer;
    protected int $transferCollelationId;

    // No real sanity checking here, and PHP typing should do
    // everything we need it to here.
    public string $tags;
    public string $comment;

    public function __construct(Account $account, int $expense, Category $category, string $tags, ?int $subcategory, ?int $externalId=null){
        $this->id = 0;
        $this->externalId = $externalId;
        $this->account = $account;
        $this->expense = $expense;
        $this->category = $category;
        $this->tags = $tags;
        $this->subCategoryId = $subcategory;
        $this->comment = "";
    }

    protected static function constructFromDbRow(array $row): Transaction{
        $acct = Account::getAccount($row['account_id']);
        $category = Category::getCategory($row['category_id']);
        $tx = new Transaction($acct, $row['expense'], $category, $row['tags'], null);

        $tx->id = $row['id'];
        $tx->isTranfer = $row['is_transfer'];
        $tx->transferCorrelationId = $row['transfer_correlation'];

        return $tx;
    }

    public static function getDB(): DB{
        if(is_null(Self::$db)){
            Self::$db = DB::getUserTransactionDB();
        }
        return Self::$db;
    }

    public static function getTransactions(): array{
        $db = Self::getDB();

        $stmt = $db->prepare('SELECT * FROM Transactions');
        $r = $stmt->execute();

        $accts = array();
        while($row = $r->fetchArray(true)){
            $accts[] = Self::constructFromDbRow($row);
        }

        return $accts;
    }

    public static function getTransaction($id): Transaction{
        $db = Self::getDB();

        $stmt = $db->prepare('SELECT * FROM Transactions WHERE id=:id');
        $stmt->bindValue(':id', $id);
        $r = $stmt->execute();

        $row = $r->fetchArray(true);
        return Self::constructFromDbRow($row);
    }

    public function associate(Transaction $associatedTransaction): void{

    }

    public function save(): void{
        if($this->id == 0){
            $this->add();
        }else{
            $this->update();
        }
    }

    public function add(): void{
        $db = Self::getDB();

        $q = 'INSERT INTO Transactions (external_id, account_id, expense, category_id, sub_category_id, tags, is_transfer, transfer_correlation, comment)
                                VALUES (0, :account_id, :expense, :category, :subcat, :tags, :transfer, :tcorr, :comment)';
        $stmt = $db->prepare($q);

        $stmt->bindValue(':account_id', $this->account->getId());
        $stmt->bindValue(':expense', $this->expense);
        $stmt->bindValue(':category', $this->category->getId());
        $stmt->bindValue(':subcat', "");
        $stmt->bindValue(':tags', $this->tags);
        $stmt->bindValue(':transfer', false);
        $stmt->bindValue(':tcorr', null);
        $stmt->bindValue(':comment', "asdf");

        $stmt->execute();
    }

    public function update(): void{
        $db = Self::getDB();
        
        $stmt = $db->prepare('UPDATE Transactions SET account_id=:account_id, expense=:expense, category_id=:category, sub_category_id=:subcat, 
                              tags=:tags, is_transfer=:transfer, transfer_correlation=:tcorr, comment=:comment WHERE id=:id');

        $stmt->bindValue(':id', $this->id);     
        $stmt->bindValue(':account_id', $this->account->getId());
        $stmt->bindValue(':expense', $this->expense);
        $stmt->bindValue(':category', $this->category->getId());
        $stmt->bindValue(':subcat', "");
        $stmt->bindValue(':tags', $this->tags);
        $stmt->bindValue(':transfer', false);
        $stmt->bindValue(':tcorr', null);
        $stmt->bindValue(':comment', "asdf");

        $result = $stmt->execute();
    }

    public function delete(): void{
        $db = Self::getDB();

        $stmt = $db->prepare('DELETE FROM Transactions WHERE id=:id');
        $stmt->bindValue(':id', $this->id);
        
        $stmt->execute();
    }

    public function getExpense(): int{
        return $this->expense;
    }

    public function setExpense(int $expense){
        $this->expense = $expense;
    }

    public function getCategory(): Category{
        return $this->category;
    }

    public function setCategory(Category $cat){
        $this->category = $cat;
    }

    public function getId(): int{
        return $this->id;
    }

    public function getAccount(): Account{
        return $this->account;
    }

    public function setAccount(Account $acct){
        // TODO: we assume the account exists later. Should we check that here?
        $this->account = $acct;
    }
}