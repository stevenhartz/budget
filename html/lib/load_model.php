<?php

// WARNING: this does not do validation on paths.
// This should NEVER be called with untrusted user input
function require_model_once(string $modelName){
    if(false !== strpos($modelName, '/') ||
       false !== strpos($modelName, '\\') ||
       false !== strpos($modelName, '..')){
        trigger_error('Dangerous characers in model name, may allow directory traversal', E_USER_WARNING);
    }
    require_once __DIR__ . '/../models/' . $modelName . '.php';
}