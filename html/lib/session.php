<?php
defined('BUDGET') or die('access denied');

// This always included, and checking session lifetimes makes a lot of sense here.
// PHP's session expiration is non-deterministic because it waits for them to get
// garbage collected. To do this correctly, we need to manually manage it.

// We need Config to get loaded before we do this, so the best solution is to wrap
// this in a class and call it after load_all.php loads everything.
class Session{

    const INACTIVE_EXPIRE = 1;
    const ABSOLUTE_EXPIRE = 2;
    
    // This should be the only time session_start is called.
    // There is one exception currently for logout.php, which
    // doesn't need to check the expiration or really load
    // up a lot of code.
    // Since this is the only session_start call, we can do
    // all our session initialization here.
    public static function checkExpiration(){

        // Sessions get written to disk for persistence by default.
        // This means $_SESSION['key'], the database key is written
        // to disk.
        // We're still going to let session data be written to disk,
        // but we will encrypt the database key before that happens.
        // Additionally, we're going to intercept session destroys and
        // session garbage collects so "securely" wipe the key data.
        session_set_save_handler(new BudgetSessionHandler());

        session_start(array(
            'gc_maxlifetime', Config::session_max_lifetime,
            'cookie_lifetime', Config::session_max_lifetime
        ));

        if(isset($_SESSION['expiration'])){
            if($_SESSION['expiration'] < time()){
                self::expireCurrent();
                header('Location: /index.php?exp='.self::INACTIVE_EXPIRE);
                die();
            }
        }

        if(!isset($_SESSION['absolute_expiration'])){
            $_SESSION['absolute_expiration'] = time() + Config::session_max_lifetime;
        }elseif($_SESSION['absolute_expiration'] < time()){
            self::expireCurrent();
            header('Location: /index.php?exp='.self::ABSOLUTE_EXPIRE);
            die();
        }

        $_SESSION['expiration'] = time() + Config::session_lifetime;
    }

    public static function expireCurrent(){
        // This does NOT run if a user leaves the application before
        // their session expires without logging out. In that case,
        // there's no page call. We could use `session_set_save_handler()`
        // to handle this (and prevent session data being saved to disk!)

        BudgetSessionHandler::log('{session.php} expireCurrent called!');
        
        // PHP 7.4 and 8.0 still treat "strings" as raw byte arrays.
        // Since strings are mutable arrays, this overwrites the
        // current database key in memory.
        $len = strlen($_SESSION['key']);
        if($len > 0){
            $wipe = random_bytes($len);
            for($i=0; $i<$len; $i++){
                $_SESSION['key'][$i] = $wipe[$i];
            }
        }

        session_destroy();
    }


}