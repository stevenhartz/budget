<?php
defined('BUDGET') or die('access denied');

class TOTP{

    const SECRET_SIZE = 16; // bytes
    const DEFAULT_PERIOD = 30; //seconds
    const HMAC_ALGO = 'sha256';

    const OUTPUT_SIZE = 6; // decimal digits
    const MIN_INT_SIZE = 30; // minimum number of bits needed to encode OUTPUT_SIZE decimal digits

    protected $secret;
    protected $period;
    protected $algo;

    function getAlgo(): string{
        return $this->algo;
    }

    function getPeriod(): int{
        return $this->period;
    }

    function getSecret(): string{
        return $this->secret;
    }

    function __construct(string $secret, int $period=self::DEFAULT_PERIOD, string $algo=self::HMAC_ALGO){
        $this->secret = $secret;
        $this->period = $period;
        if(in_array($algo, hash_hmac_algos())){
            $this->algo = $algo;
        }else{
            throw new ValueError('Unrecognized algorithm `' . $algo . '`');
        }
    }

    public static function generate(): TOTP{
        // Documentation states this is a secure source of randomness on all platforms.
        $secret = random_bytes(self::SECRET_SIZE);

        return new self($secret, time());
    }

    public function getCode($counterOffset = 0): string{
        $counter = (int)(time() / $this->period) + $counterOffset;

        $_counter = pack('J', $counter);
        $hash = hash_hmac($this->algo, $_counter, $this->secret, true);

        $numeric = self::dynamic_truncate($hash);
        $calcCode = sprintf('%0'.self::OUTPUT_SIZE.'d', $numeric % 10**self::OUTPUT_SIZE);

        return $calcCode;
    }

    public function validate(string $code): bool{

        // Allows up to 30 seconds of time "drift" between client and server.
        // More importantly, this allows me a few seconds to paste the TOTP code
        // from KeePass into the form before it expires, which was annoying.
        $current = ($code === $this->getCode());
        $previous = ($code === $this->getCode(-1));

        return $current || $previous;
    }

    private static function dynamic_truncate(string $hash): int{
        // unpack returns a 1-based array! I have no idea why.
        $offset = unpack('c', $hash[-1])[1] & 0x0f;

        $number = unpack('N', substr($hash, $offset, 4))[1] & 0x7fffffff;
        return $number;
    }
}