<?php
defined('BUDGET') or die('access denied');

class CSRF{
    
    public static function getToken(){
        if(isset($_SESSION['csrf_token'])){
            $_SESSION['csrf_token_last_get'] = time();
        }else{
            $token = base64_encode(random_bytes(16));
            $_SESSION['csrf_token'] = $token;
            $_SESSION['csrf_token_last_get'] = time();
        }

        return $_SESSION['csrf_token'];
    }

    public static function checkToken($token){
        if(!isset($_SESSION['csrf_token'])){
            return false;
        }else{
            return $token == $_SESSION['csrf_token'];
        }
    }
}