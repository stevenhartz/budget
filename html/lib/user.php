<?php
defined('BUDGET') or die('access denied');

class User{
    // TODO: account lockouts

    protected $id;
    protected $username;
    protected $displayName;
    protected $dbKeySalt;
    public TOTP $totp;

    const NUM_SALT_BYTES = 16;

    public function getUserId(){
        return $this->id;
    }

    public function getDbKeySalt(){
        return $this->dbKeySalt;
    }

    public function getUsername(){
        return $this->username;
    }

    public function getDisplayName(){
        return $this->displayName;
    }

    
    // Only used internally after a checkPassword attempt
    // No reason to hit the DB twice if we already have the info
    private static function getUserFromRow($row){
        $u = new User();
        $u->id = $row['id'];
        $u->username = $row['username'];
        $u->displayName = $row['display_name'];
        $u->dbKeySalt = $row['db_key_salt'];
        $u->totp = new TOTP($row['totp_secret']);

        return $u;
    }

    public static function getUser($id){
        $stmt = DB::getUsersDB()->prepare(
            'SELECT * FROM Users WHERE id=:id'
        );
        $stmt->bindValue(':id', $id);
        $result = $stmt->execute();

        if($result !== false){
            $row = $result->fetchArray();
            $hash = $row['password'];
            
            return self::getUserFromRow($row);
        }else{
            return null;
        }
    }

    /**
     * Centralizing this check, so that the logic is the same everywhere.
     */
    public static function isLoggedIn(): bool{
        return User::isTOTPSolved() && User::isDatabaseUnlocked();
    }

    public static function isTOTPSolved(): bool{
        return isset($_SESSION['userId']) && $_SESSION['userId'] != null;
    }

    public static function isDatabaseUnlocked(): bool{
        return isset($_SESSION['key']) && $_SESSION['key'] != null;
    }

    /**
     * Convenience method so that everything about users
     * lives in this class.
     */
    public static function getCurrentUser(): User{
        if(self::isTOTPSolved()){
            return self::getUser($_SESSION['userId']);
        }

        throw new AuthenticationError("User not logged in (TOTP not solved)");
    }

    /* Deprecated
     * This should now be obsolete. Instead, passwords are used
     * to unlock the database. If the database unlocks, the
     * password is correct.
     * */
    public static function checkPassword($user, $pass): bool{
        
        $stmt = DB::getUsersDB()->prepare(
            'SELECT * FROM Users WHERE username=:user'
        );
        $stmt->bindValue(':user', $user);
        $result = $stmt->execute();

        if($result !== false ){
            if(false !== ($row = $result->fetchArray())){
                $hash = $row['password'];
                
                $verify = password_verify($pass, $hash);
                if($verify){
                    $_SESSION['userId'] = $row['id'];
                    return true;
                }
            }else{
                // hash the given password to prevent timing sidechannel attacks.
                // the salt here is completely irrelevant, since we'll never check the answer,
                // but it does need to be the correct length.
                self::password_hash($pass, str_pad('', self::NUM_SALT_BYTES, 'a'));
            }
        }
        
            
        return false;
    }

    public static function checkTOTP($user, $code): bool{
        $stmt = DB::getUsersDB()->prepare(
            'SELECT * FROM Users WHERE username=:user'
        );
        $stmt->bindValue(':user', $user);
        $result = $stmt->execute();

        if($result !== false ){
            if(false !== ($row = $result->fetchArray())){
                $secret = $row['totp_secret'];
                $totp = new TOTP($secret);

                if($totp->validate($code)){
                    // Users must still provide their passphrase to unlock the database
                    // so this isn't a "complete" login yet.
                    $_SESSION['userId'] = $row['id'];
                    Logger::getLogger()->logSecurity("Successful TOTP login for user {$user}");

                    return true;
                }else{
                    Logger::getLogger()->logSecurity("Failed TOTP login for user {$user}");
                }
            }
        }
        
            
        return false;
    }

    public static function register($user, $pass): User{
        if(strlen($pass) < Config::pw_min_len){
            throw new Exception('Password too short (min length: ' . Config::pw_min_len .')');
        }elseif(strlen($pass) > Config::pw_max_len){
            throw new Exception('Password too long (max length: ' . Config::pw_max_len .')');
        }

        $pwSalt = random_bytes(self::NUM_SALT_BYTES);
        $dbSalt = DB::createDbKeySalt();

        $hash = self::password_hash($pass, $pwSalt);

        $totp = TOTP::generate();

        $db = DB::getUsersDB();
        $stmt = $db->prepare(
            'INSERT INTO Users (id, is_admin, is_locked, username, display_name, password, db_key_salt, totp_secret)
             VALUES (null, false, false, :user, :display, :pw, :dbsalt, :totpsec)'
        );
        $stmt->bindValue(':user', $user);
        $stmt->bindValue(':display', $user); // TODO: display name?
        $stmt->bindValue(':pw', $hash);
        $stmt->bindValue(':dbsalt', $dbSalt);
        $stmt->bindValue(':totpsec', $totp->getSecret());

        $result = $stmt->execute();
        if($result === false){
            
            // TODO: fix this username enumeration.
            throw new Exception('Failed to register this user account. ('.$db->lastErrorMsg().')');
        }

        Logger::getLogger()->logSecurity("Created a new user account: `$user`");
        $_SESSION['userId'] = $db->lastInsertRowId();
        return self::getCurrentUser();
    }

    /* Deprecated */
    // This exists so that I can prevent checkPassword timing User Enumeration.
    // I need the ability to hash a checkPassword password when no user is found
    // so that it's not obvious when a username is not registered.
    // It also means register() and checkPassword()-no-user use the same code.
    private static function password_hash($pass, $pwSalt){
        return password_hash($pass, PASSWORD_ARGON2ID,array(
            "salt" => $pwSalt,
            "memory_cost" => Config::argon2_memory_cost,
            "time_cost" => Config::argon2_time_cost,
            "threads" => Config::argon2_parallelism
        ));
    }
 
}

class AuthenticationError extends Exception{

}