CREATE TABLE Transactions (
    id INTEGER NOT NULL PRIMARY KEY,
    external_id TEXT NOT NULL, -- Used to deduplicate transactions from e.g. Chase
    account_id int NOT NULL,
    expense REAL NOT NULL, --Consider fixed-point representation?
    category_id INTEGER NOT NULL,
    sub_category_id INTEGER NOT NULL,
    tags TEXT NOT NULL,
    is_transfer BOOLEAN NOT NULL,
    transfer_correlation INT, --allow null here for non-transfers
    comment TEXT NOT NULL
);

CREATE TABLE Categories (
    id INTEGER NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    budget REAL NOT NULL,
    expense BOOLEAN NOT NULL
);
--Example categories:
INSERT INTO Categories (id, name, budget, expense) VALUES (null, "Housing", 120000, 1);
INSERT INTO Categories (id, name, budget, expense) VALUES (null, "Utilities", 18000, 1);
INSERT INTO Categories (id, name, budget, expense) VALUES (null, "Salary", 120000, 0);


CREATE TABLE SubCategories (
    id INTEGER NOT NULL PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE Accounts (
    id INTEGER NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    external_id TEXT NOT NULL -- Used for stuff like Chase's account number, different for each card
);

CREATE TABLE Balances (
    id INTEGER NOT NULL PRIMARY KEY,
    account_id INTEGER NOT NULL,
    balance REAL NOT NULL,
    date_ts INTEGER NOT NULL,
    manually_entered BOOLEAN NOT NULL
);

CREATE TABLE Recurring (
    id INTEGER NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    description TEXT NOT NULL,
    recurrence TEXT NOT NULL,
    paycycle_months INTEGER NOT NULL,
    amount REAL NOT NULL
);

CREATE TABLE Goals (
    id INTEGER NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    target_amount REAL NOT NULL,
    target_date_ts INTEGER NOT NULL,
    account_id INTEGER NOT NULL,
    monthly_contrib REAL NOT NULL
);

COMMIT;