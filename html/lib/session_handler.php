<?php
defined('BUDGET') or die('access denied');

class BudgetSessionHandler implements SessionHandlerInterface{

    
    private const LOGPATH = '/var/www/sess.log';
    private const APCU_KEY = 'session_key';
    private const CIPHER = 'aes-128-gcm';
    private const TAG_LEN = 16; // Akways 16 bytes for GCM

    private $key;

    private $sessDir;

    public static function log($msg){
        // Has log-injection vuln.
        $d = date('Y/m/d h:m:s');

        file_put_contents(self::LOGPATH, "[${d}]: ${msg}\n", FILE_APPEND);
    }

    private function sessFileName($sessId){
        return $this->sessDir . '/sess_' . $sessId;
    }

    private function encrypt($data){
        $ivlen = openssl_cipher_iv_length(self::CIPHER);
        $iv = random_bytes($ivlen);

        $ciphertext = openssl_encrypt($data, self::CIPHER, $this->key, OPENSSL_RAW_DATA, $iv, $tag);
        return $iv . $tag . $ciphertext;
    }

    private function decrypt($data){
        $ivlen = openssl_cipher_iv_length(self::CIPHER);
        $iv = substr($data, 0, $ivlen);
        $tag = substr($data, $ivlen, self::TAG_LEN);

        $ciphertext = substr($data, $ivlen + self::TAG_LEN);

        $plaintext = openssl_decrypt($ciphertext, self::CIPHER, $this->key, OPENSSL_RAW_DATA, $iv, $tag);
        if($plaintext !== false){
            return $plaintext;
        }else{
            // Could happen if this file is manipulated, but
            // WILL happen if the server is restarted!
            throw new SessionDecryptException();
        }
    }

    public function __construct(){
        if(!apcu_exists(self::APCU_KEY)){
            apcu_add(self::APCU_KEY, random_bytes(16));
        }

        $this->key = apcu_fetch(self::APCU_KEY);
    }

    public function open($sessPath, $sessName){
        $this->sessDir = $sessPath;
        return true;
    }

    public function close(){

        if(isset($_SESSION['key'])){
            $keylen = strlen($_SESSION['key']);

            Logger::getLogger()->logDebug('wiping key');

            if($keylen > 0){
                $wipe = random_bytes($keylen);
                for($i = 0; $i < $keylen; $i++){
                    $_SESSION['key'][$i] = $wipe[$i];
                }
            }

            Logger::getLogger()->logDebug("Session key after wiping: ".$_SESSION['key']);
        }

        return true;
    }

    public function read($sessId){
        
        $filePath = $this->sessFileName($sessId);

        if(!file_exists($filePath)){
            self::log('Session file did not exist');
            // Happens when a session is new, and file_get_contents
            // generates a warning when the file doesn't exist.
            return '';
        }

        $data = file_get_contents($filePath);

        // Catch either false or ''
        if($data != false){

            try{
                $data = $this->decrypt($data);

                Logger::getLogger()->logDebug("Session data decrypted: ".$data);

                return $data;
            }catch(SessionDecryptException $e){
                // There's no possible recovery
                self::log("Failed to decrypt session $sessId. Was the server restarted?");
                return '';
            }
        }else{
            return '';
        }
    }

    public function write($sessId, $data){

        Logger::getLogger()->logDebug("Session data encrypted: " . $data);

        $filePath = $this->sessFileName($sessId);
        file_put_contents($filePath, $this->encrypt($data));

        return true;
    }

    public function destroy($sessId){

        unlink($this->sessFileName($sessId));
        return true;
    }

    public function gc($lifetime){
        self::log('GC called');
        return true;
    }
}

class SessionDecryptException extends Exception {}