<?php


class Logger{

    const FILE_PATH = __DIR__ . '/../../log.log';
    private static ?Logger $instance = null;

    private function __construct(){
        $this->file = fopen(self::FILE_PATH, 'a');
    }

    public static function getLogger(): Logger{
        if(self::$instance == null){
            self::$instance = new Logger();
        }

        return self::$instance;
    }

    private function log(string $level, string $message){
        // Newlines seperate log entries, so do something about those
        // to mitigate log forgery
        $message = str_replace("\n", "[\\n]", $message);
        fwrite($this->file, "[$level] [".date('c')."] $message\n");
    }

    public function logDebug($message){
        if(defined('DEBUG')){
            $this->log('DEBUG', '[' . $_SERVER['REQUEST_URI'] . '] ' . $message);
        }
    }

    public function logInfo($message){
        $this->log('INFO', $message);
    }

    public function logError($message){
        $this->log('ERROR', $message);
    }

    public function logSecurity($message){
        $this->log('SECURITY', $message);
    }
}