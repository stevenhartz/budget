<?php
defined('BUDGET') or die('access denied');

class DB{

    // singletons
    private static $userDB = null;

    // TODO: make this a proper singleton
    private static $transactionDBs = array();
    
    private $filename;
    public $db;
    private $keySalt;

    // Indicates that we SHOULD NOT save the database.
    // Used to prevent __destruct() calls when db unlock
    // fails, because this kills the database
    private $safeToDestruct;

    // We need to save this, because the setup cost it too expensive to
    // pay every request. We'll need to pull this in and out of $_SESSION
    private $key;

    /**
     * @param $filename     The filename to store encrypted data in
     * @param $passphrase   Human-typeable passphrase to derive encryption key from
     *                          if this is null, the key will be grabbed from $_SESSION.
     *                          this is done because key derivation should be expensive
     *                          and that would impact UX if it happened every page load.
     * @param $dbObj        Internal use only; don't import data from file use
     *                          this existing database object (Used during creation)
     */
    private function __construct(User $user, ?string $passphrase=null, ?Sqlite3 $dbObj=null){//$passphrase, $salt){

        

        // Will be set to true if the import succeeds in
        // DB::import_encrypted or directly if a db obj is passed
        $this->safeToDestruct = false;
        
        $this->filename = DB::getFilepath($user);
        
        $logger = Logger::getLogger();

        if(is_null($dbObj)){

            if(is_null($passphrase)){

                $logger->logInfo("Opening database for logged-in user `{$user->getUsername()}`");
                
                if(!isset($_SESSION['key'])){
                    throw new DbNotUnlockedException();
                }
                $this->key = $_SESSION['key'];
                $logger->logDebug("Using session key `" . $_SESSION['key'] . "`");

                $this->import_encrypted($this->filename);


            }else{
                $u = User::getCurrentUser();
                $this->keySalt = $u->getDbKeySalt();


                $this->key = DB::deriveDbKey($passphrase, $this->keySalt);
                $logger->logDebug("Trying to unlock with key `" . $this->key . "` (salt: {$this->keySalt})");
                $_SESSION['key'] = $this->key;
                $logger->logDebug("Storing db key in session: {$_SESSION['key']}");

                $this->import_encrypted($this->filename);
                $logger->logSecurity("Successfully unlocked database for user {$u->getUsername()}");

            }
        }else{
            $this->setDbKey($passphrase, $user->getDbKeySalt());
            $this->db = $dbObj;
            $this->safeToDestruct = true;
        }

    }

    public function __destruct(){
        echo '<!-- destruct -->'; // leaving this for now for debugging
        if($this->safeToDestruct){
            $this->export_encrypted();
        }
    }

    // TODO: This isn't working as I need it to. See Constructor.
    // What's important here is getting $_SESSION['key'];
    private function setDbKey($passphrase, $salt){
        $this->key = DB::deriveDbKey($passphrase, $salt);

        $_SESSION['key'] = $this->key;
        Logger::getLogger()->logDebug("Storing db key in session: `{$this->key}`");
    }

    // TODO: Do I even need this?
    private function getDbKey(){
        return $_SESSION['db_key'];
    }

    private static function deriveDbKey($textKey, $salt){
        // Uses PBKDF2 to derive an encryption key from a
        // user-typeable string
    
        $key = hash_pbkdf2(
            Config::pbkdf2_algo,
            $textKey,
            $salt,
            Config::pbkdf2_iterations,
            0,     // don't truncate output
            true   // return raw bytes not ASCII hex
        );
    
        return $key;
    }
    
    public static function createDbKeySalt(){
        $salt = base64_encode(random_bytes(16));
        return $salt;
    }
    
    public static function createTransactDB($user, $passphrase){

        $salt = $user->getDbKeySalt();
        
        $setup_script = file_get_contents(__DIR__ . '/db-scripts/create-user-db.sql');


        $db = new SQLite3(':memory:');
        $db->exec($setup_script);

        Logger::getLogger()->logDebug("Created in-memory database for user {$user->getUsername()} with a passphrase.");
        
        $db = new DB($user, $passphrase, $db);
        $db->salt = $salt;
        return $db;
    }

    private static function getFilepath($user){

        // Using user ID here so that any dangerous characters in a username
        // are not used, and ensure there cannot be conflicts. Any username
        // santiziation scheme could result in a collision.
        return Config::db_dir . '/' . $user->getUserId() . '-transactions.sqlite';
    }

    public static function unlockAndGetTransactDB($user, $passphrase){
        Self::$transactionDBs[$user->getUserId()] = new DB($user, $passphrase);
        return Self::$transactionDBs[$user->getUserId()];
    }
    
    public static function getTransactDB(User $user){

        if(!isset(Self::$transactionDBs[$user->getUserId()])){
            Self::$transactionDBs[$user->getUserId()] = new DB($user);
        }

        return Self::$transactionDBs[$user->getUserId()];
    }

    /* Simple method to get the currently logged-in user's transaction DB.
     * Will throw errors if the user isn't logged in, the db hasn't been
     * unlocked, or anything goes wrong, really.
     * */
    public static function getUserTransactionDB(){

        if(!User::isLoggedIn()){
            throw new Exception('User not logged in');
        }else{

            $u = User::getCurrentUser();
            try{
                $db = DB::getTransactDB($u);
                if(!is_null($db)){
                    return $db;
                }
                
            }
            catch(DbNotUnlockedException $e){
                throw new Exception('Not unlocked!');
            }catch(DbDecryptFailedException $e){
                throw new Exception('Data unlock failed or is corrupt');
            }

            throw Exception("DB was null for some reason?");
        }
    }

    public static function getUsersDB(){
        if(self::$userDB === null){
            self::$userDB = new SQLite3(Config::db_dir . '/users.sqlite');
        }
        return self::$userDB;
    }

    public static function export($db){
        // NOTE: SQLite documentation states that this is deprecated
        // and it should be sqlite_schema. However, this is not the
        // case with my current implementation and this is a backwards
        // compatible equivalent. This may break some day.
        $r = $db->query('SELECT name,sql FROM sqlite_master');

        // We need to keep data and structure separate for parameterization.
        // Otherwise, we end up with cases like `some"thing"` being an acceptable
        // value when it's received by a page that becomes dangerous when the db
        // is recreated.
        $return = array('sql' => '', 'inserts' => array());

        $tables = array();

        while(false !== ($table = $r->fetchArray())){
            $tables[] = $table['name'];
            $return['sql'] .= $table['sql'] . ";\n";
        }

        foreach($tables as $table){
            // CANNOT be parameterized, so don't add dangerous table names
            $r = $db->query("SELECT * FROM ${table}");
            
            if(!is_null($r)){
                $inserts = array();
                while(false !== ($row = $r->fetchArray(SQLITE3_ASSOC))){
                    $cols = array_keys($row);
                    $params = preg_filter('/^/', ':', $cols); // turns `param` into `:param` for every element of $cols

                    $col_list = implode(',', $cols);
                    $param_list = implode(',', $params);
                    $vals  = array_values($row);
                    
                    $named_vals = array_combine($params, $vals);
                    
                    $q = "INSERT INTO ${table} (${col_list}) VALUES ($param_list);\n";
                    $return['inserts'][] = array('query'=> $q, 'data'=> $named_vals);
                }
            }
            
        }

        return $return;
    }

    private function export_encrypted(){

        if(in_array(Config::db_crypt_cipher, openssl_get_cipher_methods())){
            $db_json = json_encode(DB::export($this->db));

            Logger::getLogger()->logDebug("Closing Database with key {$this->key} (salt {$this->salt})");

            $ivlen = openssl_cipher_iv_length(Config::db_crypt_cipher);
            $iv = openssl_random_pseudo_bytes($ivlen);
            $ciphertext = openssl_encrypt($db_json, Config::db_crypt_cipher, $this->key, 0, $iv, $tag);

            $this->tag = $tag;

            // Saving cipher type allows us to swap algorithms later
            $serialized = json_encode(array(
                "cipher" => Config::db_crypt_cipher,
                "iv" => base64_encode($iv),
                "ciphertext" => $ciphertext,
                "tag" => base64_encode($tag)
            ));
            
            file_put_contents($this->filename, $serialized);

        }else{
            throw new Exception('Unable to use the openssl cipher (Config::db_crypt_cipher) because it is not available.');
        }
    }

    private function import_encrypted(){

        $serialized = file_get_contents($this->filename);

        $deserialized = json_decode($serialized, true);

        $ivlen = openssl_cipher_iv_length($deserialized['cipher']);
        $iv = base64_decode($deserialized['iv']);
        if(strlen($iv) != $ivlen){
            throw new Exception('IV length mismatch');
        }

        $db_json = openssl_decrypt(
            $deserialized['ciphertext'],
            $deserialized['cipher'],
            $this->key,
            0,
            base64_decode($deserialized['iv']),
            base64_decode($deserialized['tag'])
        );

        if(strlen($db_json) == 0){
            $this->safeToDestruct = false;
            throw new DbDecryptFailedException('Failed to decrypt database or it is encrypted');
        }else{
            echo '<!--' . htmlspecialchars($plaintext) . '-->';
            $this->safeToDestruct = true;
        }

        $db_data = json_decode($db_json, true);

        $this->db = new SQLite3(':memory:');
        $this->db->exec($db_data['sql']);

        foreach($db_data['inserts'] as $insert){
            $stmt = $this->db->prepare($insert['query']);
            
            foreach($insert['data'] as $name=>$val){
                $stmt->bindValue($name, $val);
            }

            $stmt->execute();
        }


    }

    /**
     * Pass-thru methods for SQLite3 class. I couldn't
     * find a good way to extend SQLite3, so I gotta
     * live with this.
     */
    public function query($q){
        return $this->db->query($q);
    }

    public function execute($q){
        return $this->db->execute($q);
    }

    public function prepare($q){
        return $this->db->prepare($q);
    }

    public function lastInsertRowID(){
        return $this->db->lastInsertRowID();
    }

}

DB::getUsersDB();

class DbNotUnlockedException extends Exception{}
class DbDecryptFailedException extends Exception{}