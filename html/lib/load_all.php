<?php
defined('BUDGET') or die('access denied');

require_once(__DIR__ . '/../config.php');

foreach(scandir(__DIR__) as $file){
    if($file != '.' && $file != '..'){

        $file = realpath(__DIR__ . '/' . $file);
        
        if(is_file($file)){
            // using require_once should save us from loading this file twice.
            require_once($file);
        }
    }
}

// Run this manually, because we need Config to load before we check session lifetime.
// I can't be bothered to make the imports some deterministic order so just call this.
Session::checkExpiration();