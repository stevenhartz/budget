<?php
define('BUDGET',true);
require_once('lib/load_all.php');

$was_submitted = false;
$error = null;
$u = null;
if(isset($_POST['register'])){
    $was_submitted = true;
    if($_POST['password'] != $_POST['confirm']){
        $error = 'Passwords do not match';
    }else{
        $_SESSION['tz_offset_seconds'] = $_POST['time_offset'] * 60;
        try{
            $u = User::register($_POST['username'], $_POST['password']);

            try{
                DB::createTransactDB($u, $_POST['password']);
            }catch(Exception $e){
                echo $e->getMessage();
                $error = 'An unknown error occured, please try again later.';
            }
            //header('Location: db_setup.php');
            //exit();
        }catch(Exception $e){
            $error = $e->getMessage();
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Budget</title>
    <?php require('reuseables/unauth_styles_scripts.php') ?>
    <script src="scripts/register.js"></script>
</head>
<body>
    <?php require('reuseables/header.php') ?>
    <div id="container">
        <div id="login">
            <h2>Register</h2>
            <p>Please provide a username and a passphrase.</p>
            <?php if(is_null($u)) { ?>       
                <form action="" method="post">
                    <p class="errorTxt"><?php
                    if(!is_null($error)){
                        // I don't actually know if there's any chance of dangerous characters here...
                        echo htmlentities($error);
                    }
                    ?></p>
                    <input type="text" name="username" placeholder="Username" <?php if($was_submitted) echo 'value='.htmlentities($_POST['username'])?> />
                    <p>Your passphrase is used to encrypt all financial data, so make sure it is long and difficult to guess. It <strong>cannot</strong> be recovered, so consider storing it in a password manager such as KeePass or LastPass.</p>
                    <input id="password" type="password" name="password" placeholder="Passphrase" />
                    <input id="confirm" type="password" name="confirm" placeholder="Passphrase" onkeyup="checkMatch()" />
                    <input type="submit" name="register" value="register" />
                </form>
            <?php }else{ ?>
                <p>Please record the following 2FA information in your authenticator:</p>
                <ul>
                    <li>Base64 TOTP Secret: <?php echo htmlspecialchars(base64_encode($u->totp->getSecret())) ?></li>
                    <li>TOTP Period: <?php echo htmlspecialchars($u->totp->getPeriod()) ?> seconds</li>
                    <li>TOTP Algorithm: <?php echo htmlspecialchars($u->totp->getAlgo()) ?></li>
                </ul>
                <p>The current and next TOTP codes are:</p>
                <ul>
                    <li><?php echo $u->totp->getCode() ?></li>
                    <li><?php echo $u->totp->getCode($counterOffset=1) ?></li>
                </ul>
                <a href='index.php'>Continue</a>
            <?php } ?>
        </div>
    </div>
    <?php require('reuseables/footer.php') ?>
</body>
</html>