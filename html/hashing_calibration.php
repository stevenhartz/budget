<html>
    <head>
        <style>
            thead td{
                text-align: left;
            }
            tbody td{
                text-align: right;
            }
        </style>
    </head>
    <body>
    <form action="" method="POST">
        <label for="time">Target Time</label>
        <br />
        <input name="time" id="time" type="text" placeholder="seconds" />
        <br />
        <input name="test" type="submit" value="Start Testing" />
    </form>
    <p>Testing may take some time.
    <?php

    set_time_limit(1200);
    function single_test($m, $t, $threads){
        
        $testPassword = 'DoNotUseWeakPasswords';


        $start = microtime(true);
        $p = password_hash($testPassword, PASSWORD_ARGON2ID,array(
            "memory_cost" => $m,
            "time_cost" => $t,
            "threads" => $threads
        ));
        $end = microtime(true);

        return $end - $start;
    }

    if(isset($_POST['test'])){
    

        $results = array();

        $total_time_start = microtime(true);

        
        {
            $threads = 12;

            for($_m=128; $_m<=2048; $_m*=2){
                $m = $_m * 512;

                for($t = 1; $t < 20; $t++){
                    $time = single_test($m, $t, $threads);

                    // Really hacky workaround for floats as keys, so
                    // we can sort by time later. PHP's array sorting sucks.
                    $results[$time * 10000000] = array(
                        'time' => $time,
                        'm' => $m,
                        't' => $t,
                        'threads' => $threads
                    );
                    //echo $time . 's => (t=' . $t . ', m=' . $m . 'KiB, threads=' . $threads . ")\n";
                }
            }
        }

        $total_time_stop = microtime(true);

        echo 'Total elapsed time: ' . ($total_time_stop - $total_time_start) . 's';

        ksort($results);

        echo '<table>';

        echo '<tr>';
        echo '<td><b>Calc Time</b></td>';
        echo '<td><b>Time Factor</b></td>';
        echo '<td><b>Memory</b></td>';
        echo '<td><b>Threads</b></td>';

        foreach($results as $time => $params){
            echo '<tr>';

            $target = $_POST['time'];
            $t1 = .9 * $target;
            $t2 = 1.1 * $target;

            if($t1 < $params['time'] && $params['time'] < $t2){
                echo '<td><b>' . $params['time'] . '</b></td>';
            }else{
                echo '<td>' . $params['time'] . '</td>';
            }

            echo '<td>' . $params['t'] . '</td>';

            $m = ($params['m'] > 1024 ? $params['m'] / 1024 . ' MiB' : $params['m'] . 'KiB' );
            echo '<td>' . $m . '</td>';
            
            echo '<td>' . $params['threads'] . '</td>';
            echo '<tr>';
        }


        echo '</body></table>';
    }
    ?>
    </body>
</html>