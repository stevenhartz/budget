<?php
define('BUDGET',true);
require_once('lib/load_all.php');

$error = null;
if(User::isLoggedIn()){
    if(isset($_POST['decrypt'])){
        $u = User::getCurrentUser();

        try{
            $db = DB::unlockAndGetTransactDB($u, $_POST['passphrase']);
            header('Location: /dashboard.php');
        }catch(DbDecryptFailedException $e){
            $error = 'Failed to unlock database';
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Budget</title>
    <?php require('reuseables/unauth_styles_scripts.php') ?>
    <script src="scripts/register.js"></script>
</head>
<body>
    <?php require('reuseables/header.php') ?>
    <div id="container">
        <div id="login">
            <h2>Database key</h2>
            <p>
                Please enter your database decryption key.
            </p>

            <?php if(!is_null($error)){
                echo '<p class="error">' . $error . '</p>';
            }?>
            <form action="" method="post">
                <input id="password" type="password" name="passphrase" />
                <input type="submit" name="decrypt" value="Unlock" />
            </form>
        </div>
    </div>
    <?php require('reuseables/footer.php') ?>
</body>
</html>
<?php

}else{
    header('Location: /index.php');
}
