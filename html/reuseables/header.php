<?php
defined('BUDGET') or die('access denied')
?>
    <header>
        <a href="/"><h1>Budget</h1></a>
        <?php if(User::isLoggedIn()){ ?>
        <div id="nav">
            {{nav}}
        </div>
        <?php } ?>
    </header>
