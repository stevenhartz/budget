<?php
defined('BUDGET') or die('Access Denied');
?>

<?php

/**
 * @param $expense_or_income     true  => expense, false => income
 * @param $dashboard             true  => show transactions totals and percentage of total expenses
 *                               false => show add/remove controls
 */
function echo_categories_table($expense_or_income, $dashboard=false){
    global $db;
?>
    <table>
        <thead>
            <tr>
                <td class="category">Category</td>
                <td>Budget</td>
                <?php
                if($dashboard){
                    echo '<td>Spending</td>';
                    echo '<td><!-- Percent --></td>';
                }else{
                    echo '<td><!-- Add/Remove --></td>';
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            if($expense_or_income){
                if($dashboard){
                    $q = 'SELECT id, name, budget, (SELECT sum(budget) from Categories WHERE expense=true) as total 
                          FROM Categories WHERE expense=true';
                }else{
                    $q = 'SELECT id, name, budget 
                          FROM Categories WHERE expense=true';
                }
            }else{
                if($dashboard){
                    $q = 'SELECT id, name, budget, (SELECT sum(budget) from Categories WHERE expense=false) as total 
                          FROM Categories WHERE expense=false';
                }else{
                    $q = 'SELECT id, name, budget 
                          FROM Categories WHERE expense=false';
                }
            }

            $cssStub = ($expense_or_income? 'budget' : 'income');
            
            $r = $db->query($q);
            // Reserving row 0 for the "add" functionality
            $row_i = 1;
            while($row = $r->fetchArray(true)){

                $id = $row['id'];
                echo '<tr>';
                $safeName = htmlentities($row['name']);
                echo "<td class='category'><span class='input_hidden edit_hidden' id='hidden_${cssStub}_cat${id}'>${safeName}</span><input class='edit_text edit_text_${cssStub}_cat' id='text_${cssStub}_cat${id}' type='text' value='$safeName' /></td>";

                $budget = sprintf('%.2f',$row['budget'] / 100.0);
                echo "<td class='${cssStub}'><span class='input_hidden edit_hidden' id='hidden_${cssStub}_num${id}'>${budget}</span><input class='edit_text edit_text_${cssStub}_num' id='text_${cssStub}${id}' type='text' value='${budget}' /></td>";

                if($dashboard){
                    // TODO: fetch transactions amount
                    echo '    <td class="percent top3">???</td>';

                    $percent = sprintf('%.1f', ($row['budget'] / $row['total'] *100.0));
                    echo '    <td class="percent top3">'.$percent.'%</td>';
                }else{
                    echo "<td>A<img id='update_${cssStub}_row${id}' class='icon_disabled a icon' disabled='disabled' src='/img/icons/update.svg' alt='update'>&nbsp;<img id='remove_budget_row${id}' class='icon' src='/img/icons/delete.svg' alt='delete'></td>";
                    echo '</tr>';
                }
                
            }
            
            if(!$dashboard){
                echo '<tr>';
                echo "<td class='category' ><span class='input_hidden add_hidden' id='hidden_${cssStub}_cat0'></span><input class='add_text add_text_${cssStub}_cat' id='text_${cssStub}_cat0' type='text' placeholder='New...' /></td>";
                echo "<td><span class='input_hidden add_hidden' id='hidden_${cssStub}_num0'></span><input class='add_text add_text_${cssStub}' id='text_${cssStub}_num0' type='text' placeholder='New...' /></td>";
                echo "<td><img id='add_${cssStub}_row0' class='icon' src='img/icons/add.svg' alt='add'/></td>";
                    
                echo '</tr>';
                
            }
            ?>
        </tbody>
    </table>
    <script src="/scripts/categories.js"></script>
    
    <?php

}
