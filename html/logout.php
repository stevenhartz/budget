<?php
define('BUDGET', 1);

require_once('lib/load_all.php');

try{
    $u = User::getCurrentUser();
    Logger::getLogger()->logSecurity("Logout: user `{$u->getUsername()}`");
}catch(Exception){}

BudgetSessionHandler::log('{Logout} start');

Session::expireCurrent();


BudgetSessionHandler::log('{Logout} stop');

header('Location: /index.php');
