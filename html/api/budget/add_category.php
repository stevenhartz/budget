<?php
define('BUDGET',true);
require_once('../../lib/load_all.php');

// Storing is fine, but clients should not reuse the response value
//header('Cache-Control: no-cache');

if(!User::isLoggedIn()){
    die('{"error":"Not logged in"}');
}else{
    $u = User::getCurrentUser();

    $SUCCESS = 'success';
    $ERROR = 'error';

    $result = $SUCCESS;
    $errMsg = '';
    $missing = '';
    $extra = '';
    $id = '';

    if(is_null($u)){
        $result = $ERROR;
    }

    try{
        DB::getTransactDB($u);
    }catch(DbNotUnlockedException $e){
        $result = $ERROR;
        $errMsg = 'Database not unlocked';
    }catch(DbDecryptFailedException $e){
        $success = $ERROR;
        $errMsg = 'Database not decrypted - may be corrupt';
        
    }

    $json = file_get_contents('php://input');
    $data = json_decode($json, true);



    if(!isset($data['csrf'])){
        $result = $ERROR;
        $errMsg = 'CSRF token not present';
        $missing = 'csrf';
    }elseif(!CSRF::checkToken($data['csrf'])){
        $result = $ERROR;
        $errMsg = 'CSRF token validation failure';
    }

    if(!isset($data['name'])){
        $result = $ERROR;
        $errMsg = 'Name missing';
        $missing = 'name';
    }elseif(strlen($data['name']) < 1){
        $result = $ERROR;
        $errMsg = "Name shouldn't be blank";
    }

    if(!isset($data['budget'])){
        $result = $ERROR;
        $errMsg = 'Budget missing';
        $missing = 'budget';
    }elseif(!preg_match('/^[0-9]+(\\.[0-9]+)?$/', $data['budget'])){
        $result = $ERROR;
        $errMsg = 'Budget invalid (non-numeric)';
    }

    /*
    $stmt = $db->prepare('INSERT INTO Categories (id, name, budget) VALUES (null, :name, :budget');
    $stmt->bindValue(':name', $data['name']);
    $stmt->bindValue(':budget', $data['budget']);

    $stmt->execute();

    die('{"success":"true"}');
    */

    // DEBUG
    $id = random_int(99, 200);
    echo json_encode(Array(
        "result" => $result,
        "error_message" => $errMsg,
        "missing" => $missing,
        "extra" => $extra,
        "id" => $id
    ));

}