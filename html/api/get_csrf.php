<?php
define('BUDGET',true);
require_once('../lib/load_all.php');

if(!User::isLoggedIn()){
    die('{"error":"Not logged in"}');
}else{
    die('{"csrf":"' . CSRF::getToken() . '"}');
}