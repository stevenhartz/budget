<?php
define('BUDGET',true);
require_once('lib/load_all.php');

if(User::isLoggedIn()){
    if(isset($_POST['encrypt'])){
        if($_POST['passphrase'] != $_POST['confirm']){
            $error = 'Passphrases do not match';
        }elseif(strlen($_POST['passphrase']) < 14){
            $error = 'Passphrase must be at least 14 characters';
        }else{
            $u = User::getCurrentUser();

            try{
                DB::createTransactDB($u, $_POST['passphrase']);
                echo 'OK!';
                header('Location: /dashboard.php');
            }catch(Exception $e){
                echo $e->getMessage();
                $error = 'An unknown error occured, please try again later.';
            }
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Budget</title>
    <?php require('reuseables/unauth_styles_scripts.php') ?>
    <script src="scripts/register.js"></script>
</head>
<body>
    <?php require('reuseables/header.php') ?>
    <div id="container">
        <div id="login">
            <h2>Database key</h2>
            <p>
                The key below will be used to encrypt your database. Please choose a 
                strong, memorable passphrase. Passphrases must be at least 14 characters
                long, but longer is better.
            </p>
            <p>
                <strong>
                    We cannot help you recover forgotten database keys. Consider using
                    a password manager, such as KeePass, LastPass, or OnePass, to save
                    both your password and this value.
                </strong>
            </p>

            <?php if(isset($error)){
                echo '<p class="error">' . $error . '</p>';
            }?>
            <form action="" method="post">
                <input id="password" type="password" name="passphrase" />
                <input id="confirm" type="password" name="confirm" oninput="checkMatch()"/>
                <input type="submit" name="encrypt" value="Set Key" />
            </form>
        </div>
    </div>
    <?php require('reuseables/footer.php') ?>
</body>
</html>
<?php

}else{
    header('Location: /index.php');
}
