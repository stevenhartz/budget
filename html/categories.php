<?php
define('BUDGET',true);
require_once('lib/load_all.php');

if(!User::isLoggedIn()){
    header('Location: /index.php');
}else{
    $u = User::getCurrentUser();
}

require_model_once('category');

if(isset($_POST['submit']) || isset($_POST['submit_x'])){

    //TODO: fix assumption that everything is set

    $errors = array();
    if(strlen($_POST['name']) == 0){
        $errors[] = "Category name blank";
    }

    if(!preg_match('/^\\d+(\\.\\d\\d)?$/', $_POST['budget'])){
        $errors[] = "Budget value invalid, should resemble '123' or '12.34'";
    }

    if(count($errors) == 0){

        // converts to boolean for db
        $type = ($_POST['type'] == 'expense' ? Category::Expense : Category::Income);
        $budget = round($_POST['budget'] * 100);

        if($_POST['id'] == 0){
            // add new
            $category = new Category($_POST['name'], $budget, $type);
            $category->save();
        }else{
            // update existing
            try{
                $category = Category::getCategory($_POST['id']);
                $category->setName($_POST['name']);
                $category->setBudget($budget);
                $category->setType($type);

                $category->save();
            }catch(CategoryNotFoundException $e){
                $errors[] = 'Category id does not exist.';
            }
        }
    }


}elseif(isset($_POST['delete']) || isset($_POST['delete_x'])){
    Category::deleteCategory($_POST['id']);
}

require('reuseables/categories.php'); 
?>
<!DOCTYPE html>	
<html>
<head>
    <title>Budget</title>
    <?php require('reuseables/unauth_styles_scripts.php') ?>
    <?php require('reuseables/auth_styles_scripts.php') ?>
</head>
<body class="darkmode">
    <header>
        <div id="logo">
            <a href="/"><h1>Budget</h1></a>
        </div>
        <div id="profile">
            <ul>
                <li>Welcome, <?php echo htmlentities($u->getUsername()) ?></li>
                <li><a href="profile.php">Profile</a></li>
                <li><a href="logout.php">Log Out</a></li>
            </ul>
        </div>
        <div class="clear"></div>
    </header>
    <div id="container">
        <div id="tabs">
            <ul id="tab-nav">
                <li>
                    <a href="/dashboard.php">
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="/transactions.php">
                        <span>Transactions</span>
                    </a>
                </li>
                <li>
                    <a href="/accounts.php">
                        <span>Accounts</span>
                    </a>
                </li>
                <li>
                    <a href="/goals.php">
                        <span>Goals</span>
                    </a>
                </li>
                <li class="selected">
                    <a href="/categories.php">
                        <span>Categories</span>
                    </a>
                </li>
                <div class="clear"></div>
            </ul>
        </div>
        <div id="main" class="category-page">
            <?php
            if(isset($errors) && count($errors) > 0){
                ?>
                <div style="position:absolute; top:0; left: 45%; background-color: red;">
                    <?php foreach($errors as $error){
                        echo '<p>'.$error.'</p>';
                    }?>
                </div>
                <?php
            }?>
            <div class="left dash-column">
                <div id="expenses">
                    <h2>Expenses</h2>
                    <div class="table">
                        <div class="tr">
                            <div class="th category">Category</div>
                            <div class="th">Budget</div>
                            <div class="th"></div>
                        </div>
                <?php
                    $categories = Category::getExpenseCategories();
                    foreach($categories as $category){
                        ?>

                        <form class="tr" action="" method="POST">
                            <div class="td category">
                                <input type="text" name="name" value="<?php echo htmlentities($category->getName()) ?>" />
                            </div>
                            <div class="td">
                                <?php $fixedPointBudget = $category->getBudget();
                                $budget = sprintf("%.2f", $fixedPointBudget / 100.0); ?>
                                <input name="budget" type="text" value="<?php echo htmlentities($budget) ?>" />
                            </div>
                            
                            <div class="td">
                                <input type="image" name="submit" src="/img/icons/update.svg" class="icon"  title="Update"/>
                                <input type="image" name="delete" src="/img/icons/delete.svg" class="icon"  title="Delete"/>
                                <input type="hidden" name="id" value="<?php echo $category->getId() ?>" />
                                <input type="hidden" name="type" value="expense" />
                            </div>
                        </form>
                    
                        
                        <?php
                    }   ?>
                        
                        <div class="tr">
                            <div class="td header-add">Add New</div>
                        </div>
                        <form class="tr" action="" method="POST">
                            <div class="td category">
                                <?php if(isset($errors) && count($errors) > 0 && $_POST['id']==0){ ?>
                                    <input type="text" name="name" value="<?php echo htmlentities($_POST['name']) ?>" />
                                <?php }else{ ?>
                                    <input type="text" name="name" placeholder="Category" />
                                <?php } ?>
                            </div>
                            <div class="td">
                                <?php if(isset($errors) && count($errors) > 0 && $_POST['id']==0){ ?>
                                    <input type="text" name="budget" value="<?php echo htmlentities($_POST['budget']) ?>" />
                                <?php }else{ ?>
                                    <input name="budget" type="text" placeholder="0.00" />
                                <?php } ?>
                            </div>
                            
                            <div class="td">
                                <input type="image" name="submit" src="/img/icons/add.svg" class="icon"  title="Add"/>
                                <input type="hidden" name="id" value="0" />
                                <input type="hidden" name="type" value="expense" />
                            </div>
                        </form>
                    
                    </div>

                </div>
            </div>
            <div class="right dash-column categories-page">
                <div id="income">
                <h2>Income</h2>
                    <div class="table">
                        <div class="tr">
                            <div class="th category">Category</div>
                            <div class="th">Budget</div>
                            <div class="th"></div>
                        </div>
                <?php
                    $categories = Category::getIncomeCategories();
                    foreach($categories as $category){
                        ?>

                        <form class="tr" action="" method="POST">
                            <div class="td category">
                                <input type="text" name="name" value="<?php echo htmlentities($category->getName()) ?>" />
                            </div>
                            <div class="td">
                                <?php $fixedPointBudget = $category->getBudget();
                                $budget = sprintf("%.2f", $fixedPointBudget / 100.0); ?>
                                <input name="budget" type="text" value="<?php echo htmlentities($budget) ?>" />
                            </div>
                            
                            <div class="td">
                                <input type="image" name="submit" src="/img/icons/update.svg" class="icon"  title="Update"/>
                                <input type="image" name="delete" src="/img/icons/delete.svg" class="icon"  title="Delete"/>
                                <input type="hidden" name="id" value="<?php echo $category->getId() ?>" />
                                <input type="hidden" name="type" value="income" />
                            </div>
                        </form>
                    
                        
                        <?php
                    }   ?>
                        
                        <div class="tr">
                            <div class="td header-add">Add New</div>
                        </div>
                        <form class="tr" action="" method="POST">
                            <div class="td category">
                                <?php if(isset($errors) && count($errors) > 0 && $_POST['id']==0){ ?>
                                    <input type="text" name="name" value="<?php echo htmlentities($_POST['name']) ?>" />
                                <?php }else{ ?>
                                    <input type="text" name="name" placeholder="Category" />
                                <?php } ?>
                            </div>
                            <div class="td">
                                <?php if(isset($errors) && count($errors) > 0 && $_POST['id']==0){ ?>
                                    <input type="text" name="budget" value="<?php echo htmlentities($_POST['budget']) ?>" />
                                <?php }else{ ?>
                                    <input name="budget" type="text" placeholder="0.00" />
                                <?php } ?>
                            </div>
                            
                            <div class="td">
                                <input type="image" name="submit" src="/img/icons/add.svg" class="icon"  title="Add"/>
                                <input type="hidden" name="id" value="0" />
                                <input type="hidden" name="type" value="income" />
                            </div>
                        </form>
                    
                    </div>

                </div>

            </div>
            <div class="clear"></div>
        </div>
    </div>
    <script type="text/javascript" src="/scripts/dashboard.js"></script>
</body>
</html>