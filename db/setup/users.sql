CREATE TABLE Users (
    id INTEGER NOT NULL PRIMARY KEY,
    is_admin BOOLEAN NOT NULL,
    is_locked BOOLEAN NOT NULL,
    username TEXT NOT NULL UNIQUE,
    display_name TEXT NOT NULL,
    password BLOB NOT NULL,
    db_key_salt BLOB NOT NULL,
    totp_secret BLOB NOT NULL
);


